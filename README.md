# xuanjin001


<!--
**xuanjin001/xuanjin001** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

https://github.com/anuraghazra/github-readme-stats#github-stats-card
--> 

- 🔭 I’m currently working on Salesforce coding
- 🌱 I’m currently learning VueJS and Mapbox JS
- 👯 I’m looking to collaborate on anything coding, I want to learn everything, but I am interested in VueJS related items. 
- 🤔 I’m looking for help with ... I will let you know. 
- 💬 Ask me about SFDC implementation and SFDC flow, automation
- 📫 How to reach me: @xjin530, twitter is the best way!! ping me!! 
- 😄 Pronouns: She/Her
- ⚡ Fun fact: hard problems excites me  

![Xuan's GitHub stats](https://github-readme-stats.vercel.app/api?username=xuanjin001&count_private=true)

[XuanJin's Github](https://github.com/xuanjin001)
